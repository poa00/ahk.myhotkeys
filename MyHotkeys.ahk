﻿; MyHotkeys.ahk
;
; Useful common day-to-day use hotstrings.


; General configuration.
#SingleInstance force
Menu, Tray, Icon, MyHotkeys.ico


; Function to create an UUID.
CreateUUID() {
    VarSetCapacity(puuid, 16, 0)
    if !(DllCall("rpcrt4.dll\UuidCreate", "ptr", &puuid))
        if !(DllCall("rpcrt4.dll\UuidToString", "ptr", &puuid, "uint*", suuid))
            return StrGet(suuid), DllCall("rpcrt4.dll\RpcStringFree", "uint*", suuid)

    return ""
}


; Key remappings.
AppsKey::RAlt ; Useful in my laptop, where I usually hit the `Menu` key instead of `AltGr`.


; HTML hotstrings.
:*?::em::<em></em>{Left 5}
:*?::strong::<strong></strong>{Left 9}
:*?::ul::<ul>{Enter}<li></li>{Enter}</ul>{Up}{End}{Left 5}
:*?::ol::<ol>{Enter}<li></li>{Enter}</ol>{Up}{End}{Left 5}
:*?::li::<li></li>{Left 5}
:*?::eng::<cite lang="en"></cite>{Left 7}
:*?::esp::<cite lang="es"></cite>{Left 7}
:*?::lat::<cite lang="la"></cite>{Left 7}
:*?::fra::<cite lang="fr"></cite>{Left 7}
:*?::deu::<cite lang="de"></cite>{Left 7}
:*?::que::<cite lang="quenya"></cite>{Left 7}
:*?::ps::<strong>PS</strong>{Space}
:*?::pps::<strong>PPS</strong>{Space}
:*?::ppps::<strong>PPPS</strong>{Space}
:*?::acro::<acronym title=""></acronym>{Left 12}
:*?::dele::<del></del>{Left 6}
:*?::subi::<sub></sub>{Left 6}
:*?::supi::<sup></sup>{Left 6}
:*?::code::<code></code>{Left 7}
:*?::table::<table>{Enter}{Enter}</table>{Up}
:*?::tr::<tr>{Enter}{Enter}</tr>{Up}
:*?::td::<td></td>{Left 5}
:*?::th::<th></th>{Left 5}


; Wikimedia hotstrings.
:*?::wcode::<code class="hcc"></code>{Left 7}

:*?::wimg::
uuid := createUUID()
SendInput, <div id="%uuid%"></div>[[File:|left|frame|<strong>Ilustración 1:</strong>]]{Enter}<div style="clear: both"></div>{Left 77}
return

:*?::wtable::<div id="tabla1" style="border: 1px solid {U+0023}CCCCCC; background: {U+0023}FBFBFB; padding: 1px 15px 1px 15px; display: inline-block;">{Enter}{Enter}{Enter}<p style="font-size: 90%; margin-top: 0; margin-bottom: 0; background: {U+0023}EEEEEE; margin-left: -15px; margin-right: -15px; padding-left: 15px;">'''Tabla 1''': </p>{Enter}</div>


; Hotkeys for characters in barbaric languages.

; Left-pointing double angle quotation mark («).
; https://www.fileformat.info/info/unicode/char/ab/index.htm
^!z::
Send, {U+00AB}
return

; Right-pointing double angle quotation mark (»).
; https://www.fileformat.info/info/unicode/char/bb/index.htm
^!x::
Send, {U+00BB}
return

; Latin small letter sharp s (ß).
; https://en.wikipedia.org/wiki/%C3%9F
; https://www.fileformat.info/info/unicode/char/00df/index.htm
^!s::
Send, {U+00DF}
return

; Latin small ligature oe (œ).
; https://www.fileformat.info/info/unicode/char/153/index.htm
:*?C::oe::
Send, {U+0153}
return

; Latin capital ligature OE (Œ).
; https://www.fileformat.info/info/unicode/char/152/index.htm
:*?C::OE::
Send, {U+0152}
return

; Latin small letter ae (æ).
; https://www.fileformat.info/info/unicode/char/e6/index.htm
:*?C::ae::
Send, {U+00E6}
return

; Latin capital letter AE (Æ).
; https://www.fileformat.info/info/unicode/char/c6/index.htm
:*?C::AE::
Send, {U+00C6}
return

; Latin small letter a with ring above (å).
; https://www.fileformat.info/info/unicode/char/e5/index.htm
:*?C::aring::
Send, {U+00E5}
return

; Latin capital letter A with ring above (Å).
; https://www.fileformat.info/info/unicode/char/c5/index.htm
:*?C::Aring::
Send, {U+00C5}
return

; Latin small letter o with stroke (ø).
; https://www.fileformat.info/info/unicode/char/f8/index.htm
:*?C::ob::
Send, {U+00F8}
return

; Latin capital letter O with stroke (Ø).
; https://www.fileformat.info/info/unicode/char/d8/index.htm
:*?C::OB::
Send, {U+00D8}
return

; Latin small letter eth (ð).
; https://www.fileformat.info/info/unicode/char/f0/index.htm
:*?C::eth::
Send, {U+00F0}
return

; Latin capital letter Eth (Ð).
; https://www.fileformat.info/info/unicode/char/00D0/index.htm
:*?C::Eth::
Send, {U+00D0}
return

; Greek small letter alpha (α).
; https://www.fileformat.info/info/unicode/char/3b1/index.htm
:*?C::alfa::
Send, {U+03B1}
return

; Greek small letter beta (β).
; https://www.fileformat.info/info/unicode/char/3b2/index.htm
:*?C::beta::
Send, {U+03B2}
return

; Greek small letter gamma (γ).
; https://www.fileformat.info/info/unicode/char/03b3/index.htm
:*?C::gamma::
Send, {U+03B3}
return

; Greek small letter delta (δ).
; https://www.fileformat.info/info/unicode/char/3b4/index.htm
:*?C::delta::
Send, {U+03B4}
return

; Greek small letter epsilon (ε).
; https://www.fileformat.info/info/unicode/char/3b5/index.htm
:*?C::epsilon::
Send, {U+03B5}
return

; Greek small letter mu (μ).
; https://www.fileformat.info/info/unicode/char/3bc/index.htm
:*?C::mu::
Send, {U+03BC}
return

; Trade Mark sign (™).
; https://www.fileformat.info/info/unicode/char/2122/index.htm
:*?C::tm::
Send, {U+2122}
return

; Rightwards arrow (→).
; https://www.fileformat.info/info/unicode/char/2192/index.htm
:*?C::rarr::
Send, {U+2192}
return

; Leftwards arrow (←).
; https://www.fileformat.info/info/unicode/char/2190/index.htm
:*?C::larr::
Send, {U+2190}
return

; Hammer and sickle (☭).
; https://fileformat.info/info/unicode/char/262d/index.htm
:*?C::cccp::
Send, {U+262D}
return

; Biohazard sign (☣).
; https://www.fileformat.info/info/unicode/char/2623/index.htm
:*?C::bio::
Send, {U+2623}
return

; Skull and crossbones (☠).
; https://www.fileformat.info/info/unicode/char/2620/index.htm
:*?C::skull::
Send, {U+2620}
return

; Radioactive sign (☢).
; https://www.fileformat.info/info/unicode/char/2622/index.htm
:*?C::radio::
Send, {U+2622}
return

; Atom symbol (⚛).
; https://www.fileformat.info/info/unicode/char/269b/index.htm
:*?C::atom::
Send, {U+269B}
return

; Not equal to (≠).
; https://www.fileformat.info/info/unicode/char/2260/index.htm
:*?C::neq::
Send, {U+2260}
return

; White heart suit (♡).
; https://www.fileformat.info/info/unicode/char/2661/index.htm
:*?C::heart::
Send, {U+2661}
return

; Han character "Happiness, good fortune, blessing" (福).
; https://www.fileformat.info/info/unicode/char/798f/index.htm
:*?C::fu::
Send, {U+798F}
return


; Adjusts Windows Explorer columns with optimal sizes in my 65% keyboard.
^+::
Send, ^{NumpadAdd}
return


; Date and time.
:*?::hoy::
FormatTime,TimeString,,dd/MM/yyyy
Send %TimeString%
return

:*?::yoh::
FormatTime,TimeString,,yyyyMMdd
Send %TimeString%
return


; Hard to classify hotstrings.
:*?:xD::
Send, {U+00D7}D
return

:*?::shrug::
Send, ¯\_(ツ)_/¯
return


; Markdown hotstrings.
:*?::lnk::
Send, [``[]``](){Left 1}
return


; SQL hotstrings.
:*?::todate::
FormatTime,TimeString,,dd/MM/yyyy
Send, TO_DATE ('%TimeString% 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
return


; Additional stuff, hard to classify.
:*?::confirm::
Send,Buenos días.{Enter}{Enter}El equipo de desarrollo ha comprobado que el despliegue se ha realizado con éxito, por lo que puede darse por finalizada la solicitud.{Enter}{Enter}Gracias y saludos.

:*?::uuid::
uuid := createUUID()
SendInput, %uuid%
return